<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:table');
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Editpage:edit');
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Editpage:new');
		return $router;
	}

}

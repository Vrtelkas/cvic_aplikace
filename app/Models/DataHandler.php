<?php
/**
 * Created by PhpStorm.
 * User: VRTAK
 * Date: 25.06.2016
 * Time: 16:48
 */

namespace App\Models;

use Nette;
use Nette\Database\Connection;
use Nette\Security as NS;

class DataHandler extends Nette\Object {

    protected $database;

    public function __construct()
    {
        $this->database = new Connection('mysql:host=sql7.freesqldatabase.com;dbname=sql7126750','sql7126750','xT1jtKfBBH');
    }

    public function getData($page)
    {
        $result = $this->database->query("SELECT * FROM access LIMIT ?,10", $page*10-10)->fetchAll();

        $table = array();
        foreach($result as $row){
            $table[] = [
                'Username' => $row['Username'],
                'Password' => $row['Password'],
                'Pohlavi' => $row['Gender'],
                'ID' => $row['ID']
            ];
        }
        return $table;
    }

    public function editRow(array $newData)
    {
        list($id, $username, $password, $gender) = $newData;

        $hash = NS\Passwords::hash($password);

        if($gender === 0){
            $gender = "muz";
        } else {$gender = "zena";}

        $this->database->query("UPDATE access SET Username = ?, Password = ?, Gender = ? WHERE ID = ?",
            $username, $hash, $gender, $id );

    }

    public function addRow(array $data)
    {
        list($username, $password, $gender) = $data;

        $hash = NS\Passwords::hash($password);

        if($gender === 0){
            $gender = "muz";
        } else {$gender = "zena";}

        $this->database->query("INSERT INTO access (Username, Password, Gender) VALUES(?, ?, ?)", $username, $hash, $gender);

    }

    public function getNumberOfRows(array $filter = NULL)
    {
        if(!isset($filter)){
            $result = $this->database->query("SELECT * FROM access")->fetchAll();
            return count($result);
        } else {
            list($search, $select) = $filter;

            $selectValue = [
                0 => 'vse',
                1 => 'zena',
                2 => 'muz'
            ];

            if ($select == 0) {
                $result = $this->database->query("SELECT * FROM access WHERE Username LIKE '%{$search}%'")->fetchAll();
                return count($result);
            } else {
                $result = $this->database->query("SELECT * FROM access WHERE Username LIKE '%{$search}%' AND Gender = ?", $selectValue[$select])->fetchAll();
                return count($result);
            }
        }

    }

    public function filterData(array $filter, $page)
    {
        list($search, $select) = $filter;

        $selectValue = [
            0 => 'vse',
            1 => 'zena',
            2 => 'muz'
        ];

        if ($select == 0) {
            $result = $this->database->query("SELECT * FROM access WHERE Username LIKE '%{$search}%' LIMIT ?,10", $page*10-10)->fetchAll();

            $table = array();
            foreach($result as $row){
                $table[] = [
                    'Username' => $row['Username'],
                    'Password' => $row['Password'],
                    'Pohlavi' => $row['Gender'],
                    'ID' => $row['ID']
                ];
            }

        } else {
            $result = $this->database->query("SELECT * FROM access WHERE Username LIKE '%{$search}%' AND Gender = ? LIMIT ?,10",
                $selectValue[$select], $page*10-10)->fetchAll();

            $table = array();
            foreach($result as $row){
                $table[] = [
                    'Username' => $row['Username'],
                    'Password' => $row['Password'],
                    'Pohlavi' => $row['Gender'],
                    'ID' => $row['ID']
                ];
            }
        }
            return $table;
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: VRTAK
 * Date: 23.06.2016
 * Time: 20:29
 */
namespace App\Models;

use Nette\Security as NS;
use Nette\Database\Connection;
use Nette;

class LoginHandler extends Nette\Object implements NS\IAuthenticator {

    protected $database;

    public function __construct()
    {
        $this->database = new Connection('mysql:host=sql7.freesqldatabase.com;dbname=sql7126750','sql7126750','xT1jtKfBBH');
}
    public function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;

        $row = $this->database->query("SELECT username, password FROM access WHERE username = ?",
            $username)->fetch();

        if(!$row) {
            return false;
        }

        if(!NS\Passwords::verify($password, $row->password)) {
            return false;
        }

        return true;
    }


}
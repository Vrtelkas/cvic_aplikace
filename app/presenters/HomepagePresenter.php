<?php

namespace App\Presenters;


use Nette\Application\UI as UI;
use App\Models\LoginHandler as LoginHandler;
use App\Models\DataHandler as DataHandler;
use Nette\Utils\Paginator;
use Nette\Http;


class HomepagePresenter extends UI\Presenter
{

    protected function createComponentLogInForm()
    {
        $form = new UI\Form;
        $form->addText('name', 'Username:');
        $form->addPassword('password', 'Password:');
        $form->addSubmit('login', 'Log In');
        $form->onSuccess[] = [$this, 'logInFormSucceeded'];
        return $form;
    }

    protected function createComponentFilterForm()
    {
        $form = new UI\Form;
        $form->addText('search', '');
        $form->addSelect('filter', '', ['vse', 'zena', 'muz']);
        $form->addSubmit('submit', 'Vyhledat');
        $form->onSuccess[] = [$this, 'filterFormSucceeded'];
        return $form;
    }

    public function renderTable($success, $page, array $filter = NULL){
        if (!$success){
            $this->redirect('Homepage:default');
        }

        $paginator = new Paginator();
        $data = new DataHandler();

        if(!isset($filter)) {
            $table = $data->getData($page);
            $paginator->setItemCount($data->getNumberOfRows());
            $this->template->filter = NULL;
        } else {
            $table = $data->filterData($filter, $page);
            $paginator->setItemCount($data->getNumberOfRows($filter));
            $this->template->filter = $filter;
        }

        $paginator->setItemsPerPage(10);
        $paginator->setPage($page);

        $this->template->table = $table;
        $this->template->paginator = $paginator;
        $this->template->success = $success;
        $this->template->page = $page;

    }

    public function logInFormSucceeded(UI\form $form)
    {
        $login = new LoginHandler();
        $credentials = array();


        foreach($form->values as $value){
            $credentials[] = $value;
        }

        $success = $login->authenticate($credentials);

        $this->redirect('Homepage:table', $success, 1);

    }

    public function filterFormSucceeded(UI\form $form)
    {
        $filter = array();

        foreach($form->values as $value){
            $filter[] = $value;
        }

        $this->redirect('Homepage:table', true, 1, $filter);
    }
}

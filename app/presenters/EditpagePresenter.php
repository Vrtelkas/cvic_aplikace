<?php
/**
 * Created by PhpStorm.
 * User: VRTAK
 * Date: 26.06.2016
 * Time: 13:37
 */

namespace App\Presenters;
use Nette\Application\UI as UI;
use App\Models\DataHandler as DataHandler;



class EditpagePresenter extends UI\Presenter{
    protected $id;
    protected $username;

    public function createComponentEditForm()
    {
        $form = new UI\Form;
        $form->addHidden('id', $this->id);
        $form->addText('username', 'Username:')->setDefaultValue($this->username);
        $form->addText('password', 'Password:');
        $form->addSelect('gender', 'Pohlavi:', ['muz', 'zena']);
        $form->addSubmit('save', 'Ok');
        $form->onSuccess[] = [$this, 'editFormSucceeded'];
        return $form;
    }

    public function createComponentNewForm()
    {
        $form = new UI\Form;
        $form->addText('username', 'Username:');
        $form->addText('password', 'Password:');
        $form->addSelect('gender', 'Pohlavi:', ['muz', 'zena']);
        $form->addSubmit('save', 'Ok');
        $form->onSuccess[] = [$this, 'newFormSucceeded'];
        return $form;
    }

    public function renderEdit(array $row)
    {
        $this->username = $row['Username'];
        $this->id = $row['ID'];
    }


    public function editFormSucceeded(UI\Form $form)
    {
        $edit = new DataHandler();
        $values = array();

        foreach($form->values as $value) {
            $values[] = $value;
        }

        $edit->editRow($values);
        $this->redirect('Homepage:table', true, 1);
    }

    public function newFormSucceeded(UI\Form $form)
    {
        $new = new DataHandler();
        $values = array();

        foreach($form->values as $value) {
            $values[] = $value;
        }

        $new->addRow($values);
        $this->redirect('Homepage:table', true, 1);

    }
}